#!/home/pavan/workspace/virtual_envs/tensorflow/bin/python3
def add(arg1, arg2): # args are interchangeable
    return arg1 + arg2

def main():
    a, b = 3, 5
    print("sum up : ("+ str(a) +" + " + str(b) + ")" + str(add(a, b)))
    return 0

if __name__ == "__main__":
    main()